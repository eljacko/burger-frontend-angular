import { TestBed } from '@angular/core/testing';

import { SnackService } from './snack.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';

describe('SnackService', () => {
  let service: SnackService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ MatSnackBarModule ]
    }).compileComponents();
    service = TestBed.inject(SnackService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
