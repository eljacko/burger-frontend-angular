import { Injectable } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { VenuesResponse } from './burger-venues.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SnackService } from '../services/snack.service';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class VenuesService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient,
    private snackService: SnackService) {}


  /** GET burger-venues from the server */
  public getVenues(): Observable<VenuesResponse> {
    const url = `${environment.baseURI}/v1/burgers`;
    return this.http.get<VenuesResponse>(url)
      .pipe(
        tap(_ => {
          if (!environment.production) {
            console.log('fetched burger burger-venues');
          }
        }),
        catchError(this.handleError<VenuesResponse>('getVenues', {burgers: [], error: true}))
      );
  }

  private handleError<T>(operation = 'operation', result?: T): (error: any) => Observable<T> {
    return (error: any): Observable<T> => {
      if (!environment.production) {
        console.error(error);
      }
      this.snackService.showMessage(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

}


