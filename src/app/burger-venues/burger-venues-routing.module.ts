import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VenuesListPageComponent } from './venues-list-page/venues-list-page.component';

const routes: Routes = [
  { path: '', component: VenuesListPageComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BurgerVenuesRoutingModule { }
