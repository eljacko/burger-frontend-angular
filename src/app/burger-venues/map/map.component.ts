import { Component, Input, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { BurgerVenue } from '../burger-venues.model';
import { GoogleMap, MapAnchorPoint, MapInfoWindow, MapMarker } from '@angular/google-maps';

@Component({
  selector: 'app-venues-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnChanges {
  @Input() venues: BurgerVenue[] = [];
  @ViewChild(GoogleMap, {static: false}) map: GoogleMap;
  @ViewChild(MapInfoWindow, {static: false}) infoWindow: MapInfoWindow;
  markers$: BurgerVenue[] = [];
  apiLoaded: Observable<boolean>;
  infoWindowContent = '';

  markerOptions: google.maps.MarkerOptions = {
    draggable: false,
    clickable: true
  };
  mapOptions: google.maps.MapOptions = {
    center: {
      lat: 58.38062,
      lng: 26.72509
    },
    zoom: 13,
    disableDefaultUI: true,
    minZoom: 12

  };
  circleOptions: google.maps.CircleOptions = {
    center: {
      lat: 58.37802148507438,
      lng: 26.731988704678255
    },
    radius: 1000,
    clickable: false,
    strokeOpacity: 0.6,
    strokeWeight: 1
  };

  constructor(httpClient: HttpClient) {
    this.apiLoaded = httpClient.jsonp(
      'https://maps.googleapis.com/maps/api/js?key=AIzaSyATLlrcKKSf-kWlJMQXyyuYq1VXH_2kiF8&libraries=geometry', 'callback')
      .pipe(
        map(() => true),
        catchError(() => of(false)),
      );
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes.venues.currentValue.length) {
      return;
    }
    const bounds = new google.maps.LatLngBounds();
    this.markers$ = [];
    for (const venue of changes.venues.currentValue) {
      const centerLatlng = new google.maps.LatLng(this.circleOptions.center.lat as number, this.circleOptions.center.lng as number);
      const markerLatLng = new google.maps.LatLng(venue.location.lat as number, venue.location.lng as number);
      const distance = google.maps.geometry.spherical.computeDistanceBetween(markerLatLng, centerLatlng);
      if (distance > 1000) {
        this.markers$.push(venue);
        bounds.extend(markerLatLng);
      }
    }
    this.map.fitBounds(bounds);
  }

  openInfoWindow(marker: any, content: string): void {
    this.infoWindowContent = content;
    this.infoWindow.open(marker);
  }
}
