import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VenuesListPageComponent } from './venues-list-page/venues-list-page.component';
import { PhotosComponent } from './photos/photos.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { BurgerVenuesRoutingModule } from './burger-venues-routing.module';
import { HttpClientJsonpModule } from '@angular/common/http';
import { MapComponent } from './map/map.component';

@NgModule({
  declarations: [
    PhotosComponent,
    VenuesListPageComponent,
    MapComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    BurgerVenuesRoutingModule,
    CommonModule,
    HttpClientJsonpModule
  ]
})
export class BurgerVenuesModule { }
