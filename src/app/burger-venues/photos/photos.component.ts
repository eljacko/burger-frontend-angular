import { Component, Input, OnInit } from '@angular/core';
import { BurgerVenue } from '../burger-venues.model';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnInit {
  @Input() venues: BurgerVenue[] = [];
  @Input() columns: number;

  constructor() { }

  ngOnInit(): void {
  }

}
