import { Component, OnDestroy, OnInit } from '@angular/core';
import { BurgerVenue } from '../burger-venues.model';
import { VenuesService } from '../venues.service';
import { Observable, Subscription } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-venue-list',
  templateUrl: './venues-list-page.component.html',
  styleUrls: ['./venues-list-page.component.css']
})
export class VenuesListPageComponent implements OnInit, OnDestroy {

  venues: BurgerVenue[] = [];
  sub: Subscription;
  loading: boolean;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe([Breakpoints.Handset])
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private venuesService: VenuesService, private breakpointObserver: BreakpointObserver) { }

  ngOnInit(): void {
    this.loading = true;
    this.sub = this.venuesService
      .getVenues()
      .subscribe(resp => {
        this.venues = resp.burgers;
        this.loading = false;
      });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
