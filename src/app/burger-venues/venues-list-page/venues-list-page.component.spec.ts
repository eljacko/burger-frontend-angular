import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VenuesListPageComponent } from './venues-list-page.component';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { By } from '@angular/platform-browser';

describe('VenuesListPageComponent', () => {
  let component: VenuesListPageComponent;
  let fixture: ComponentFixture<VenuesListPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VenuesListPageComponent ],
      imports: [ HttpClientModule, MatSnackBarModule ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VenuesListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have a 'page-heading' container present`, () => {
    const pageHeading = fixture.debugElement.nativeElement.querySelector('.page-heading');
    expect(pageHeading).toBeTruthy();
  });

  it(`should have a heading 'Venues'`, () => {
    const heading = fixture.debugElement.query(By.css('h2')).nativeElement;
    expect(heading.innerHTML).toBe('Venues');
  });

  it(`should not have a heading 'Gathering data...'`, () => {
    const loading = fixture.debugElement.query(By.css('p')).nativeElement;
    expect(loading.innerHTML).toBe('Gathering data...');
  });

  it(`should have a 'map-element' container present`, () => {
    const pageHeading = fixture.debugElement.nativeElement.querySelector('.map-element');
    expect(pageHeading).toBeTruthy();
  });

  it(`should have a 'photos-element' container present`, () => {
    const pageHeading = fixture.debugElement.nativeElement.querySelector('.photos-element');
    expect(pageHeading).toBeTruthy();
  });

});
