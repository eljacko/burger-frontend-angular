import { TestBed } from '@angular/core/testing';

import { VenuesService } from './venues.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { VenuesResponse } from './burger-venues.model';
import { of } from 'rxjs';

describe('VenuesService', () => {
  let service: VenuesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule
      ]
    }).compileComponents();
    service = TestBed.inject(VenuesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getVenues', () => {
    it('should return burger burger-venues', () => {
      const burgerResponse: VenuesResponse = {
        burgers: []
      };
      let resp;
      spyOn(service, 'getVenues').and.returnValue(of(burgerResponse));

      service.getVenues().subscribe(res => {
        resp = res;
      });
      expect(resp).toEqual(burgerResponse);
    });
  });
});
