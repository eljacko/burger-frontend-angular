export interface VenuesResponse {
  burgers: BurgerVenue[];
  error?: boolean;
}

export interface BurgerVenue {
  name: string;
  latestPhoto?: string;
  location: google.maps.LatLngLiteral;
}
