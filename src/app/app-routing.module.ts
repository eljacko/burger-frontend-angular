import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InfoPageComponent } from './info-page/info-page.component';

const routes: Routes = [
  { path: '', component: InfoPageComponent },
  {
    path: 'burger-venues',
    loadChildren: () => import('./burger-venues/burger-venues.module')
      .then(m => m.BurgerVenuesModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
