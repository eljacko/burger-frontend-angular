export const environment = {
  production: true,
  domainUrl: 'https://burger-frontend-angular.herokuapp.com',
  baseURI: 'https://burger-app-backend.herokuapp.com'
};
