import { AppPage } from './app.po';
import { browser, logging, protractor } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;
  const EC = protractor.ExpectedConditions;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display description heading for Task', async () => {
    await page.navigateTo();
    expect(await page.getTitleText()).toEqual('Test assignment for Qminder frontend developer position');
  });

  it(`should navigate to 'Burger venues' page`, async () => {
    pending('Force skip');
    await page.goToBurgerVenues();
    const url = await browser.getCurrentUrl();
    expect(url).toBe('/burger-venues');
  });

  it(`should load venues`, async () => {
    await page.goToBurgerVenues();
    browser.waitForAngular();
    const tiles = await browser.$$('mat-grid-tile');
    expect(tiles.length).toBeGreaterThan(0);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
