import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  async getTitleText(): Promise<string> {
    return element(by.css('app-root app-shell header h1')).getText();
  }

  async openSideMenu(): Promise<unknown> {
    return  element(by.css('mat-sidenav-content mat-toolbar button mat-icon')).click();
  }

  async openSecondaryMenu(): Promise<unknown> {
    return  element(by.css('mat-toolbar button mat-icon')).click();
  }

  async goToBurgerVenues(): Promise<unknown> {
    return element(by.linkText('Burger venues')).click();
  }
}
